<%-- 
    Document   : add new users
    Created on : 08 14, 18, 2:18:40 PM
    Author     : Tim
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>WPPS Portal - Add New Users</title>

        <!-- Bootstrap core CSS-->
        <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom fonts for this template-->
        <link href="../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

        <!-- Page level plugin CSS-->
        <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

        <!-- Custom styles for this template-->
        <link href="../css/sb-admin.css" rel="stylesheet">

    </head>

    <body id="page-top">

        <nav class="navbar navbar-expand navbar-dark bg-dark static-top">

            <a class="navbar-brand mr-1" href="index.jsp.html">WPPS Web Portal</a>

            <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
                <i class="fas fa-bars"></i>
            </button>

            <!-- Navbar Search -->


        </nav>

        <div id="wrapper">

            <!-- Sidebar -->
            <ul class="sidebar navbar-nav">
                <li class="nav-item ">
                    <a class="nav-link" href="dashboard.jsp">
                        <i class="fas fa-fw fa-tachometer-alt"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="users.jsp">
                        <i class="fas fa-fw fa-folder"></i>
                        <span>Users</span>
                    </a>

                </li>
                <li class="nav-item">
                    <a class="nav-link" href="purchase_req.jsp">
                        <i class="fas fa-fw fa-chart-area"></i>
                        <span>Purchase Requisition</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="">
                        <i class="fas fa-fw fa-table"></i>
                        <span>Purchase Order</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="">
                        <i class="fas fa-fw fa-table"></i>
                        <span>Approval</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="">
                        <i class="fas fa-fw fa-table"></i>
                        <span>Budget</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="">
                        <i class="fas fa-fw fa-table"></i>
                        <span>Material</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="supplier.jsp">
                        <i class="fas fa-fw fa-table"></i>
                        <span>Supplier</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="">
                        <i class="fas fa-fw fa-table"></i>
                        <span>Invoice</span></a>
                </li>
            </ul>

            <div id="content-wrapper">

                <div class="container-fluid">

                    <!-- Breadcrumbs-->
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="#">Users</a>
                        </li>
                        <li class="breadcrumb-item active">Add New User</li>
                    </ol>

                    <!-- Area Chart Example-->
                    <div class="card md-12">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-6">
                                    <i class="fas"></i> Add New User
                                </div>

                            </div> 
                        </div>
                        <div class="card-body">
                            <form action="" method="post">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <div class="form-label-group">
                                        <input type="text" id="firstname" name="firstname" class="form-control" placeholder="First Name" required="required" autofocus="autofocus">
                                        <label for="firstname">First Name</label>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <div class="form-label-group">
                                        <input type="text" id="lastname" name="lastname" class="form-control" placeholder="Last Name" required="required" autofocus="autofocus">
                                        <label for="lastname">Last Name</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <div class="form-label-group">
                                        <input type="text" id="contactno" name="contactno" class="form-control" placeholder="Contact No." required="required" autofocus="autofocus">
                                        <label for="contactno">Contact No.</label>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <div class="form-label-group">
                                        <input type="text" id="typeofuser" name="typeofuser" class="form-control" placeholder="Type of User" required="required" autofocus="autofocus">
                                        <label for="typeofuser">Type of User</label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-6">
                                    <div class="form-label-group">
                                        <input type="text" id="email" name="email" class="form-control" placeholder="Email Address" required="required" autofocus="autofocus">
                                        <label for="email">Email Address</label>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <div class="form-label-group">
                                        <input type="password" id="password" name="password" class="form-control" placeholder="Password" required="required" autofocus="autofocus">
                                        <label for="password">Password</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                </div>
                                <div class="col-md-3">
                                    <button type="submit" class="btn btn-primary btn-block">Save</button>
                                </div>
                                <div class="col-md-3">
                                    <button type="reset" class="btn btn-primary btn-block">Clear</button>
                                </div>
                                <div class="col-md-3">
                                </div>
                            </div>
                        </form>
                        </div>
                        <div class="card-footer small text-muted">Footer</div>
                    </div>

                </div>
                <!-- /.container-fluid -->

                <!-- Sticky Footer -->
                <footer class="sticky-footer">
                    <div class="container my-auto">
                        <div class="copyright text-center my-auto">
                            <span>Copyright © WPPS Portal 2018</span>
                        </div>
                    </div>
                </footer>

            </div>
            <!-- /.content-wrapper -->

        </div>
        <!-- /#wrapper -->

        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>

        <!-- Logout Modal-->
        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                        <a class="btn btn-primary" href="login.html">Logout</a>
                    </div>
                </div>
            </div>
        </div>

        <!-- Bootstrap core JavaScript-->
        <script src="../vendor/jquery/jquery.min.js"></script>
        <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        <!-- Core plugin JavaScript-->
        <script src="../vendor/jquery-easing/jquery.easing.min.js"></script>

        <!-- Page level plugin JavaScript-->
        <script src="../vendor/chart.js/Chart.min.js"></script>
        <script src="../vendor/datatables/jquery.dataTables.js"></script>
        <script src="../vendor/datatables/dataTables.bootstrap4.js"></script>

        <!-- Custom scripts for all pages-->
        <script src="../js/sb-admin.min.js"></script>

        <!-- Demo scripts for this page-->
        <script src="../js/demo/datatables-demo.js"></script>
        <script src="../js/demo/chart-area-demo.js"></script>

    </body>

</html>