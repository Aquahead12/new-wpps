<%-- 
    Document   : purchase req
    Created on : 08 14, 18, 2:18:40 PM
    Author     : Tim
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>WPPS Portal - Purchase Requisition</title>

        <!-- Bootstrap core CSS-->
        <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom fonts for this template-->
        <link href="../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

        <!-- Page level plugin CSS-->
        <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

        <!-- Custom styles for this template-->
        <link href="../css/sb-admin.css" rel="stylesheet">
        <script>
            $(function () {

                if ($("#0").on("click", function () {
                    $("#datePickerSubmitted").datepicker('destroy');
                    $("#datePickerDue").datepicker('destroy');
                    $("#datePickerSubmitted").datepicker({
                        dateFormat: 'MM dd, yy',
                        minDate: 0,
                        maxDate: 0
                    });
                    $("#datePickerDue").datepicker({
                        dateFormat: 'MM dd, yy',
                        minDate: +1,
                        maxDate: "+1m"
                    });
                })
                        )
                    if ($("#1").on("click", function () {
                        $("#datePickerSubmitted").datepicker('destroy');
                        $("#datePickerDue").datepicker('destroy');
                        $("#datePickerSubmitted").datepicker({
                            dateFormat: 'MM dd, yy',
                            minDate: 0,
                            maxDate: 0
                        });
                        $("#datePickerDue").datepicker({
                            dateFormat: 'MM dd, yy',
                            minDate: +1,
                            maxDate: "+1w"
                        });
                    })
                            )
                        if ($("#2").on("click", function () {
                            $("#datePickerSubmitted").datepicker('destroy');
                            $("#datePickerDue").datepicker('destroy');
                            $("#datePickerSubmitted").datepicker({
                                dateFormat: 'MM dd, yy',
                                minDate: 0,
                                maxDate: 0
                            });
                            $("#datePickerDue").datepicker({
                                dateFormat: 'MM dd, yy',
                                minDate: +1,
                                maxDate: +4
                            });
                        })
                                )
                            $("#datePickerSubmitted").datepicker({
                                dateFormat: 'MM dd, yy',
                                minDate: 0,
                                maxDate: 0
                            });
                $("#datePickerDue").datepicker({
                    dateFormat: 'MM dd, yy',
                    minDate: +1,
                    maxDate: "+1m"
                });
            });
        </script>

    </head>

    <body id="page-top">

        <nav class="navbar navbar-expand navbar-dark bg-dark static-top">

            <a class="navbar-brand mr-1" href="index.jsp.html">WPPS Web Portal</a>

            <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
                <i class="fas fa-bars"></i>
            </button>

            <!-- Navbar Search -->


        </nav>

        <div id="wrapper">

            <!-- Sidebar -->
            <ul class="sidebar navbar-nav">
                <li class="nav-item ">
                    <a class="nav-link" href="dashboard.jsp">
                        <i class="fas fa-fw fa-tachometer-alt"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link" href="users.jsp">
                        <i class="fas fa-fw fa-folder"></i>
                        <span>Users</span>
                    </a>

                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="">
                        <i class="fas fa-fw fa-chart-area"></i>
                        <span>Purchase Requisition</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="">
                        <i class="fas fa-fw fa-table"></i>
                        <span>Purchase Order</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="">
                        <i class="fas fa-fw fa-table"></i>
                        <span>Approval</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="">
                        <i class="fas fa-fw fa-table"></i>
                        <span>Budget</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="">
                        <i class="fas fa-fw fa-table"></i>
                        <span>Material</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="supplier.jsp">
                        <i class="fas fa-fw fa-table"></i>
                        <span>Supplier</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="">
                        <i class="fas fa-fw fa-table"></i>
                        <span>Invoice</span></a>
                </li>
            </ul>

            <div id="content-wrapper">

                <div class="container-fluid">

                    <!-- Breadcrumbs-->
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="#">Purchase Requisitions</a>
                        </li>
                        <li class="breadcrumb-item active">Purchase Request</li>
                    </ol>

                    <!-- Area Chart Example-->
                    <div class="card md-12">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-6">
                                    <i class="fas"></i> Create Purchase Request
                                </div>
                            </div> 
                        </div>
                        <div class="card-body">
                            <form action="" method="post">
                                <div class="row">
                                    <div class="form-group col-md-3">
                                        <div class="form-label-group">
                                            <input type="text" id="req_dept" name="req_dept" class="form-control" placeholder="Requesting Department" required="required" autofocus="autofocus">
                                            <label for="req_dept">Requesting Department</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <div class="form-label-group">
                                            <input type="text" id="acc_charge" name="acc_charge" class="form-control" placeholder="Account Charge" required="required" autofocus="autofocus">
                                            <label for="acc_charge">Account Charge</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <div class="form-label-group">
                                            <input type="text" id="datePickerSubmitted" name="datePickerSubmitted" class="form-control" placeholder="Date Prepared" required="required" autofocus="autofocus">
                                            <label for="datePickerSubmitted">Date Prepared</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <div class="form-label-group">
                                            <input type="text" id="datePickerDue" name="datePickerDue" class="form-control" placeholder="Date Needed" required="required" autofocus="autofocus">
                                            <label for="datePickerDue">Date Needed</label>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <div class="form-label-group">
                                            <center>
                                                <div class="radio">
                                                    <label>Type of Purchase*</label>
                                                    <br>
                                                    <label><input type="radio" name="Regular" checked>Regular</label>
                                                    <label><input type="radio" name="Urgent">Urgent</label>
                                                    <label><input type="radio" name="Emergency">Emergency</label>
                                                </div>
                                            </center>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <div class="form-label-group">
                                            <textarea id="comment" name="comment" class="form-control" placeholder="Comment:" required="required"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <table width="100%" class="table1 table-striped table-bordered table-hover"
                                           id="dataTables-example1">

                                        <thead>
                                            <tr class="odd gradeX" style="background: #014189">
                                                <th style="color: white"></th>
                                                <th style="color: white">Item #</th>
                                                <th style="color: white">Qty</th>
                                                <th style="color: white">Unit</th>
                                                <th width="500" style="color: white">Description</th>
                                                <th style="color: white">Unit Cost</th>
                                            </tr>
                                        </thead>
                                        <tbody id="tableBody">

                                        </tbody>
                                    </table>
                                    <ul class="pagination" style="margin-left: 40%">
                                        <li><a id="prevBtn">PREV</a></li>
                                        <li><a id="nextBtn">NEXT</a></li>
                                    </ul>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                    </div>
                                    <div class="col-md-3">
                                    </div>
                                    <div class="col-md-3">
                                    </div>
                                    <div class="col-md-3">
                                        <button type="submit" class="btn btn-primary btn-block">Add</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="card-footer small text-muted">Footer</div>
                    </div>

                </div>
                <!-- /.container-fluid -->

                <!-- Sticky Footer -->
                <footer class="sticky-footer">
                    <div class="container my-auto">
                        <div class="copyright text-center my-auto">
                            <span>Copyright © WPPS Portal 2018</span>
                        </div>
                    </div>
                </footer>

            </div>
            <!-- /.content-wrapper -->

        </div>
        <!-- /#wrapper -->

        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>

        <!-- Logout Modal-->
        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                        <a class="btn btn-primary" href="login.html">Logout</a>
                    </div>
                </div>
            </div>
        </div>

        <!-- Bootstrap core JavaScript-->
        <script src="../vendor/jquery/jquery.min.js"></script>
        <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        <!-- Core plugin JavaScript-->
        <script src="../vendor/jquery-easing/jquery.easing.min.js"></script>

        <!-- Page level plugin JavaScript-->
        <script src="../vendor/chart.js/Chart.min.js"></script>
        <script src="../vendor/datatables/jquery.dataTables.js"></script>
        <script src="../vendor/datatables/dataTables.bootstrap4.js"></script>

        <!-- Custom scripts for all pages-->
        <script src="../js/sb-admin.min.js"></script>

        <!-- Demo scripts for this page-->
        <script src="../js/demo/datatables-demo.js"></script>
        <script src="../js/demo/chart-area-demo.js"></script>

        <script>
            var objToPass = [];
            var accountCharge;
            var dateDue;
            var dateSubmitted;
            var prType;
            var requestDept = $("#requestingDepartMent").attr('value');
            var purpose;
            var pagenumber = 0;

            $.ajax({
                type: 'GET',
                dataType: 'json',
                contentType: 'application/json',
                url: '/getMaterialsById',
                success: function (response) {
                    $("#prevBtn").attr("disabled", true);
                    $.each(response.materialList, function (key, value) {
                        console.log(value);
                        $('#tableBody').append('<tr class="odd gradeX"><td><input type="checkbox" class="checky" value="1"/></td>' +
                                '<td class="matId">' + value.materialId + '</td>' +
                                '<td><input type="text" value="' + value.qty + '" readonly="true" class="qty"/></td>' +
                                '<td>' + value.materialUnit + '</td>' +
                                '<td>' + value.materialDesc + '</td>' +
                                '<td><span>₱ </span>' + parseFloat(value.price).toFixed(2) + '</td></tr>')
                    })
                },
                error: function () {
                    alert("FAILED");
                }
            });

            $("#selectedAccountType").on('change', function (response) {
                pagenumber = 0;
                var selectedOption = $("#selectedAccountType option:selected").val();
                $.ajax({
                    type: 'GET',
                    dataType: 'json',
                    contentType: 'application/json',
                    url: '/getMaterialsById?id=' + selectedOption,
                    success: function (response) {
                        $('#tableBody').empty();
                        $("#prevBtn").attr("disabled", true);
                        $.each(response.materialList, function (key, value) {
                            console.log(value);
                            $('#tableBody').append('<tr class="odd gradeX"><td><input type="checkbox" class="checky" value="1"/></td>' +
                                    '<td class="matId">' + value.materialId + '</td>' +
                                    '<td><input type="text" value="' + value.qty + '" readonly="true" class="qty"/></td>' +
                                    '<td>' + value.materialUnit + '</td>' +
                                    '<td>' + value.materialDesc + '</td>' +
                                    '<td><span>₱ </span>' + parseFloat(value.price).toFixed(2) + '</td></tr>')
                        })
                    },
                    error: function () {
                        alert("FAILED");
                    }
                });
            });

            $("#nextBtn").on("click", function () {
                pagenumber++;
                var selectedOption = $("#selectedAccountType option:selected").val();
                $.ajax({
                    type: 'GET',
                    dataType: 'json',
                    contentType: 'application/json',
                    url: '/getMaterialsById?id=' + selectedOption + '&offset=' + pagenumber,
                    success: function (response) {
                        $('#tableBody').empty();
                        $.each(response.materialList, function (key, value) {
                            console.log(value);
                            $('#tableBody').append('<tr class="odd gradeX"><td><input type="checkbox" class="checky" value="1"/></td>' +
                                    '<td class="matId">' + value.materialId + '</td>' +
                                    '<td><input type="text" value="' + value.qty + '" readonly="true" class="qty"/></td>' +
                                    '<td>' + value.materialUnit + '</td>' +
                                    '<td>' + value.materialDesc + '</td>' +
                                    '<td><span>₱ </span>' + parseFloat(value.price).toFixed(2) + '</td></tr>')
                        });
                    },
                    error: function () {
                        alert("FAILED");
                    }
                });
            });

            $("#prevBtn").on("click", function () {
                pagenumber--;
                var selectedOption = $("#selectedAccountType option:selected").val();
                $.ajax({
                    type: 'GET',
                    dataType: 'json',
                    contentType: 'application/json',
                    url: '/getMaterialsById?id=' + selectedOption + '&offset=' + pagenumber,
                    success: function (response) {
                        $('#tableBody').empty();
                        $.each(response.materialList, function (key, value) {
                            console.log(value);
                            $('#tableBody').append('<tr class="odd gradeX"><td><input type="checkbox" class="checky" value="1"/></td>' +
                                    '<td class="matId">' + value.materialId + '</td>' +
                                    '<td><input type="text" value="' + value.qty + '" readonly="true" class="qty"/></td>' +
                                    '<td>' + value.materialUnit + '</td>' +
                                    '<td>' + value.materialDesc + '</td>' +
                                    '<td><span>₱ </span>' + parseFloat(value.price).toFixed(2) + '</td></tr>')
                        });
                    },
                    error: function () {
                        alert("FAILED");
                    }
                });
            });

            function formatDate(date) {
                var d = new Date(date),
                        month = '' + (d.getMonth() + 1),
                        day = '' + d.getDate(),
                        year = d.getFullYear();

                if (month.length < 2)
                    month = '0' + month;
                if (day.length < 2)
                    day = '0' + day;

                return [month, day, year].join('/');
            }



            $('#btnAddMaterial').on('click', function () {

                var checked = $('#dataTables-example1').find(':checked').length;

                if (!checked) {
                    alert("Please select materials!")
                } else {
                    $("#dataTables-example2").removeAttr("hidden");
                    $("#sendServer").removeAttr("style");

                    accountCharge = $("#selectedAccountType").val();

                    purpose = $("#purpose").val();


                    dateDue = new Date($("#datePickerDue").val());
                    dateSubmitted = new Date($("#datePickerSubmitted").val());


                    prType = $("input:radio[name=prType]:checked").val();
                    console.log("BUTTON ADD MATERIAL CLICKED");
                    var getSelectedRows = null;
                    getSelectedRows = $(".table1 input:checked").parents("tr").clone();
                    $(".table2 tbody").append(getSelectedRows);


                    var input = $('<input class="editableColumnsStyle" type="text" />');
                    $(".table2 tr").find(".checky").each(function (index, data) {
                        $(this).remove();
                    });


                    var price = Number($(".table2 tr").find(".price").html());
                    console.log(price);
                    $(".table2 tr").find(".qty").each(function (index, data) {
                        $(this).prop('readonly', false);
                        $(this).click(function () {
                            console.log($(this).val());
                            $(this).prop('type', 'number');
                            $(this).prop('max', data.value);
                            $(this).prop('min', 1);
                            $(this).bind('keyup mouseup', function (response) {
                                console.log(response);
                            });
                        });
                    });
                    $(".table1 tr").find(".checky").each(function (index, data) {
                        $(this).prop("checked", false);
                    });
                }
            });

            $('#sendServer').on("click", function () {


                if ($("#datePickerSubmitted").val() == "" || $("#datePickerDue").val() == "" || $("#purpose").val() == "") {
                    alert("Please fill up the following fields" + "\n" + "- Date Submitted" + "\n" + "- Date Due" + "\n" + "- Purpose");
                } else {
                    $(".table2").find($(".matId")).each(function (index, data1) {
                        objToPass.push({
                            material_id: data1.innerText,
                            qty: $(".table2").find($(".qty"))[index].value
                        });
                    });

                    var r = confirm("Are you sure you want to submit this request?");
                    if (r == true) {
                        sendToBackend();
                    } else {
                        console.log("you choose to cancel");
                    }
//            sendToEmail();

                }




            });

            function sendToEmail() {
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    contentType: 'application/json',
                    url: '/sendEmail?email=' + 'fifaquino@gmail.com',
                    success: function (data) {
                        alert("Email Sent!");
                    },
                    error: function () {
                        alert("FAILED");
                    }
                });
            }


            function sendToBackend() {
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    contentType: 'application/json',
                    url: '/savingPR',
                    data: JSON.stringify({
                        purchaseMaterials: objToPass,
                        accountCharge: accountCharge,
                        datePickerSubmitted: formatDate(dateSubmitted),
                        datePickerDue: formatDate(dateDue),
                        requestingDepartMent: requestDept,
                        purpose: purpose,
                        purchaseType: prType
                    }),
                    success: function (data) {
                        alert("Purhcase Request Created Successfully!");
                        getSummaryPage(data.generatedKey);
                    },
                    error: function () {
                        alert("FAILED");
                    }
                });
            }

            function getSummaryPage(requestId) {
                window.location.href = "/getSummaryDetails?requestId=" + requestId;
            }

            /*]]>*/


        </script>
    </script>
</body>

</html>